package com.example.demo;
/* 9.	Write unit tests for a program to count numbers of words in command line argument */



public class Wordcount {

	public int wordcount(String data) {

		int count = 0;
		if (data != null) {
			char ch[] = new char[data.length()];
			for (int i = 0; i < data.length(); i++) {
				ch[i] = data.charAt(i);
				if (((i > 0) && (ch[i] != ' ') && (ch[i - 1] == ' ')) || ((ch[0] != ' ') && (i == 0)))
					count++;
			}
		}
		return count;
	}

}
