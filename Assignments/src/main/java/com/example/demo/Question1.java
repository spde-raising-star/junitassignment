package com.example.demo;
/* 1.	Write unit tests for a program to show that the value of non-static variable is not visible to all the instances and therefore cannot be used to count the number of instances*/



public class Question1 {

	public int countNonStatic = 0;
	public static int countStatic = 0;

	public Question1() {
		countNonStatic++;
		countStatic++;
	}

	public static void main(String args[]) {
	}
}
