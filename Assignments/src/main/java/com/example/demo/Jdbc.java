package com.example.demo;
/* 2.	Write unit tests for JDBC program to connect the MySql database & get the count of tables & views in that database. */

//Import the packages 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Jdbc {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/student";
	static final String USER = "root";
	static final String PASS = "Sneha@1234";

	public static void main(String[] args) throws Exception {
		// Open a connection

		Class.forName(JDBC_DRIVER);

		try {
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// Database connection
			if (conn != null)
				System.out.println("Connected");
			else
				System.out.println("Not Connected");

			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS COUNT FROM PRODUCTS");

			while (rs.next()) {
				System.out.println("The count is " + rs.getInt("COUNT"));
			}

			// Create database
			String sql = "CREATE DATABASE PRODUCTS";
			stmt.executeUpdate(sql);
			System.out.println("Database created successfully...");

			System.out.println("Connected to the selected database...");

			// create table
			String sql2 = "CREATE TABLE PRODUCTS " + "(id INTEGER not NULL, " + " brand VARCHAR(255), "
					+ " pName VARCHAR(255), " + " price INTEGER, " + " PRIMARY KEY ( id ))";

			stmt.executeUpdate(sql2);
			System.out.println("Created table in given database...");

			// Insert record
			System.out.println("Inserting records into the table...");
			String sql4 = "INSERT INTO PRODUCTS VALUES (100, 'Baja', 'washing mission', 18000)";
			stmt.executeUpdate(sql4);
			String sql5 = "INSERT INTO PRODUCTS VALUES (101, 'LG', 'smartphone',20000 )";
			stmt.executeUpdate(sql5);
			String sql6 = "INSERT INTO PRODUCTS VALUES (102, 'Samsung', 'Refrigerator', 30000)";
			stmt.executeUpdate(sql6);
			String sql7 = "INSERT INTO PRODUCTS VALUES(103, 'Apple', 'laptop', 279273)";
			stmt.executeUpdate(sql7);
			System.out.println("Inserted records into the table...");

			// Update recode
			System.out.println("Update records  ");

			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}