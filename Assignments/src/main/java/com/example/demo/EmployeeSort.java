package com.example.demo;
/* 7.	Write unit tests for prepare a Employee object containing id, name & department. Sort the employee based on name. Implement the same using Lambda expression. */





import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EmployeeSort {

	public void SortEmployeesWithLambda(List<Employ> emploies) {
		System.out.print("before sort  ::SortEmployeesWithLambda");
		emploies.stream().forEach(System.out::println);
		emploies.sort((Employ e1, Employ e2) -> e1.getName().compareTo(e2.getName()));
		System.out.print("After sort  ::SortEmployeesWithLambda");
		emploies.stream().forEach(System.out::println);
	}

	public void sortEmployee(List<Employ> emploies) {
		System.out.print("before sort  ");
		emploies.stream().forEach(System.out::println);
		Collections.sort(emploies, new Comparator<Employ>() {
			@Override
			public int compare(Employ e1, Employ e2) {
				return e1.getName().compareTo(e2.getName());
			}
		});
		System.out.print("After sort  ");
		emploies.stream().forEach(System.out::println);
	}
}
