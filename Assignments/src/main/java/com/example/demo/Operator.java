package com.example.demo;
@FunctionalInterface
interface Operator<T> {
	T process(T a, T b);
}
