package com.example.demo;  
/* 3.	Write unit tests for a program to write a common data to multiple files using a single stream only?*/
  
import java.io.*;  
class InputStreamExample {    
	public static void fileData(String data) throws IOException {
		FileOutputStream stream1=new FileOutputStream("C:\\Users\\skanaparthi\\eclipse-workspace\\Assignments\\target\\abc1.txt");
		FileOutputStream stream2=new FileOutputStream("C:\\Users\\skanaparthi\\eclipse-workspace\\Assignments\\target\\abc2.txt");
		byte[] sdata=data.getBytes();
		stream1.write(sdata);
		stream2.write(sdata);
	}
	public static boolean compare() throws IOException {
		FileInputStream bStream1=new FileInputStream("C:\\Users\\skanaparthi\\eclipse-workspace\\Assignments\\target\\abc1.txt");
		FileInputStream bStream2=new FileInputStream("C:\\Users\\skanaparthi\\eclipse-workspace\\Assignments\\target\\abc2.txt");
		
		byte[] data1=bStream1.readAllBytes();
		
		byte[] data2=bStream2.readAllBytes();
		for(int i=0;i<data1.length;i++) {
			if(data1[i]!=data2[i]) {
				return false;
			}
		}
		return true;
	}
}