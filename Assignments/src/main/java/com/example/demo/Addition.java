package com.example.demo;
/* 6.	Write unit tests for prepare addition of two numbers method and implement the same using lambda expression. */
 

public class Addition {

	public int add(int a, int b) {
		return a + b;
	}

	public int addUsingLambda(int v1, int v2) {
		Operator<Integer> addOperation = (a, b) -> a + b;
		return addOperation.process(v1, v2);
	}

}
