package com.example.demo;
/* 10.	Write unit test for a program to find the duplicate characters in a string. */

import java.util.HashSet;
import java.util.Set;

public class DuplicateString {

	public static int countofDuplicate(String input) {
		int count=0;
		Set<Character> set=new HashSet<>();
		for(int i=0;i<input.length();i++) {
			
			System.out.println(input.charAt(i));
			if(set.contains(input.charAt(i))) {
				count++;
			}
			else {
				set.add(input.charAt(i));
			}
		}
		return count;
	}
}
 