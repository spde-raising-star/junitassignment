package com.example.demo;
/* 8.	Write unit tests for Create an ArrayList of integer having some values. List those and get the even numbers using stream API*/



import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamExcercise {

	public List<Integer> getEvenNumber(List<Integer> values) {
		if (values == null || values.size() == 0) {
			return values;
		}
		values.stream().forEach(System.out::println);
		return values.stream().filter(value -> value % 2 == 0).collect(Collectors.toList());

	}

}
