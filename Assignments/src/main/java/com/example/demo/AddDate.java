package com.example.demo;
/* 4.	Write unit tests for Print the current date with time zone, Add 10Hours to the existing date and print*/

import java.util.Calendar;
import java.util.Date;

public class AddDate {
	public Date addHoursToJavaUtilDate(Date date, int hours) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(Calendar.HOUR_OF_DAY, hours);
	    return calendar.getTime();
	}
}

