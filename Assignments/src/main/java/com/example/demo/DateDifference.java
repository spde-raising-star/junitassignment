package com.example.demo;
/* 7.	Write unit tests for a program to prepare a method to accept 2 dates as parameter and return the difference between those 2 dates.*/ 


import java.util.Date;

public class DateDifference {

	public long calculateDataDifferance(Date d1, Date d2) {
		if (d1 == null || d2 == null) {
			throw new RuntimeException("INVALID DATES");
		}
		return Math.abs(d1.getTime() - d2.getTime());
	}

}
