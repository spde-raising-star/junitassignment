package com.example.demo;
/* 2.	Write unit tests for a program to check the input characters for uppercase, lowercase, number of digits and other characters*/ 

public class Person {
	
	public String checkCharacter(char ch) {
		
		if (Character.isDigit(ch)) {
			return "digit";
		}else if (Character.isUpperCase(ch)) {
			return "upperCase";
		}else if (Character.isLowerCase(ch)) {
			return "lowerCase";
		}else{
			return "other";
		}	
		}

	public static void main(String[] args) {

	}
}