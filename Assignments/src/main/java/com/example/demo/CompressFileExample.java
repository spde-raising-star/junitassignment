package com.example.demo;
/* 10.	Write unit tests for a program to compress and uncompress the data of a file?*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class CompressFileExample {
public static void main(String args[]) {

try {
FileInputStream fin = new FileInputStream("abc1.txt");

FileOutputStream fout = new FileOutputStream("Compressed");
DeflaterOutputStream out = new DeflaterOutputStream(fout);

int i;
while ((i = fin.read()) != -1) {
out.write((byte) i);
out.flush();
}
System.out.println("File is compressed!!");
fin.close();
out.close();

} catch (Exception e) {
System.out.println(e);
}

try {
FileInputStream fin = new FileInputStream("Compressed");
InflaterInputStream in = new InflaterInputStream(fin);

FileOutputStream fout = new FileOutputStream("UnCompressed.txt");

int i;
while ((i = in.read()) != -1) {
fout.write((byte) i);
fout.flush();
}
System.out.println("File is Uncompressed!!");
fin.close();
fout.close();
in.close();

} catch (Exception e) {
System.out.println(e);
}
 }
}


