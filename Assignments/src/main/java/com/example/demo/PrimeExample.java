package com.example.demo;
/* 4.	Write unit tests for a program to print all the prime numbers in an array of n elements by taking command line arguments. */

import java.util.ArrayList;
import java.util.List;

public class PrimeExample {
	public List<Integer> printPrimeNumber(int[] values) {
		List<Integer> primeNumbers = new ArrayList<>();
		for (int i = 0; i < values.length; i++) {
			int num = values[i];
			if(num==0||num==1) {
				continue;
			}
			boolean flag = false;
			for (int j = 2; j <= num / 2; ++j) {
				if (num % j == 0) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				System.out.println(num + " is a prime number.");
				primeNumbers.add(num);
			}
		}
		if (primeNumbers.size() > 0) {
			System.out.println("Prime numbers are::");
			primeNumbers.stream().forEach(System.out::println);
		}
		return primeNumbers;
	}

}
