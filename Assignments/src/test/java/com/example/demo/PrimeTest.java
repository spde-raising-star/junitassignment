package com.example.demo;

import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;




public class PrimeTest {

	@Test
	public void testPrimeFor1st100() {
		int n = 100;
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = i + 1;
		}
		PrimeExample p = new PrimeExample();
		List<Integer> prime = p.printPrimeNumber(numbers);
		Assert.assertEquals(25, prime.size());
	}

	@Test
	public void testPrimeFor1st10() {
		int n = 10;
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = i + 1;
		}
		PrimeExample p = new PrimeExample();
		List<Integer> prime = p.printPrimeNumber(numbers);
		Assert.assertEquals(4, prime.size());
		Assert.assertTrue(2 == prime.get(0));
		Assert.assertTrue(3 == prime.get(1));
		Assert.assertTrue(5 == prime.get(2));
		Assert.assertTrue(7 == prime.get(3));
	}

	@Test
	public void testPrimewithZeroELements() {
		int n = 0;
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = i + 1;
		}
		PrimeExample p = new PrimeExample();
		List<Integer> prime = p.printPrimeNumber(numbers);
		Assert.assertEquals(0, prime.size());
	}

}
