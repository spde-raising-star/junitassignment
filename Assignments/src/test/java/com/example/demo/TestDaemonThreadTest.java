package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestDaemonThreadTest {

	@Test
	void test() {
		TestDaemonThread t1=new TestDaemonThread();
		t1.setDaemon(true);
		assertEquals(false,TestDaemonThread.currentThread().isDaemon());
		assertEquals(true, t1.isDaemon());
	}

}
