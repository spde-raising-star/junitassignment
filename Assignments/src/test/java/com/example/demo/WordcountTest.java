package com.example.demo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;




public class WordcountTest {

	private static final String TEST_WORD_COUNT_POISTIVE = "I AM WORKING IN INNOMINDS";
	private static final String TEST_WORD_COUNT_NEGATIVE_NO_DATA = "";
	private static final String TEST_WORD_COUNT_NEGATIVE_ONLY_SINGLE_WORD = "INNOMINDS";

	@Test
	public void testWordCount() {

		Wordcount p = new Wordcount();
		Assert.assertEquals(5, p.wordcount(TEST_WORD_COUNT_POISTIVE));

	}

	@Test
	public void testWordCountWhenInputHasNoData() {

		Wordcount p = new Wordcount();
		Assert.assertEquals(0, p.wordcount(TEST_WORD_COUNT_NEGATIVE_NO_DATA));

	}

	@Test
	public void testWordCountWhenInputHasNull() {

		Wordcount p = new Wordcount();
		Assert.assertEquals(0, p.wordcount(null));

	}

	@Test
	public void testWordCountWhenInputOnlySingleWord() {

		Wordcount p = new Wordcount();
		Assert.assertEquals(1, p.wordcount(TEST_WORD_COUNT_NEGATIVE_ONLY_SINGLE_WORD));

	}
}
