package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class personTest {

	Person personTest = new Person();

	@Test
	void testDigit() {

		assertEquals("digit", personTest.checkCharacter('2'));
	}

	@Test
	void testUpper1() {
		char ch = 'A';
		assertEquals("upperCase", personTest.checkCharacter(ch));
	}

	@Test
	void testUpper2() {
		char ch = 'A';
		assertNotEquals("lowerCase", personTest.checkCharacter(ch));
	}

	@Test
	void testLower() {
		char ch = 'a';
		assertEquals("lowerCase", personTest.checkCharacter(ch));
	}
}
