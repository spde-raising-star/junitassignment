package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.aspectj.lang.annotation.Before;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import com.mysql.cj.MysqlConnection;
import com.mysql.cj.xdevapi.Statement;

class JdbcTest {

	@Test
	public void testCloseStatementASE() throws SQLException {
		Connection con=DriverManager.getConnection(Jdbc.DB_URL, Jdbc.USER, Jdbc.PASS);
		
		Assert.assertEquals(false,con.isClosed());
		con.close();
		Assert.assertEquals(true, con.isClosed());
	}
}
