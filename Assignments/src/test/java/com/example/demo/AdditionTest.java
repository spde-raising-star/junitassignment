package com.example.demo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class AdditionTest {

	@Test
	public void addTest() {
		Addition a = new Addition();
		Assert.assertEquals(30, a.add(10, 20));

	}

	public void addTestUsinglambda() {
		Addition a = new Addition();
		Assert.assertEquals(30, a.addUsingLambda(10, 20));

	}

	@Test
	public void addTestUsingNegativeNumbers() {
		Addition a = new Addition();
		Assert.assertEquals(-30, a.add(-10, -20));

	}

	@Test
	public void addTestUsingNegativeNumbersWithlambda() {
		Addition a = new Addition();
		Assert.assertEquals(-30, a.addUsingLambda(-10, -20));

	}
}
