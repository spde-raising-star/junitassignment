package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import java.time.LocalDateTime;
import java.time.Month;

import org.junit.jupiter.api.Test;

class AddDateTest {

	@Test
	void test() {
		LocalDateTime actualDateTime = LocalDateTime
			      .of(2018, Month.JUNE, 25, 5, 0);
			    LocalDateTime expectedDateTime = LocalDateTime.
			      of(2018, Month.JUNE, 25, 15, 0);

			    assertThat(actualDateTime.plusHours(10)).isEqualTo(expectedDateTime);
			}
}
