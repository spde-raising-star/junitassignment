package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;




public class StreamExcerciseTest {
	@Test
	public void testGetEvenNumbers() {
		StreamExcercise se = new StreamExcercise();
		List<Integer> result = se.getEvenNumber(getArrayList());
		Assert.assertEquals(4, result.size());
	}

	public void testGetEvenNumbersWithEmpthList() {
		StreamExcercise se = new StreamExcercise();
		List<Integer> result = se.getEvenNumber(new ArrayList<>());
		Assert.assertEquals(0, result.size());

	}

	public void testGetEvenNumbersWithInputNull() {
		StreamExcercise se = new StreamExcercise();
		List<Integer> result = se.getEvenNumber(null);
		Assert.assertNull(result);

	}

	private List<Integer> getArrayList() {
		List<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(2);
		al.add(3);
		al.add(4);
		al.add(5);
		al.add(8);
		al.add(10);
		return al;
	}

}
