package com.example.demo;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

class InputStreamExampleTest {
	
	
	@Test
	   void test() throws IOException{
		Scanner scan=new Scanner(System.in);
		String data=scan.nextLine();
		InputStreamExample.fileData(data);
		
		assertEquals(true,InputStreamExample.compare());
		
	}
}
