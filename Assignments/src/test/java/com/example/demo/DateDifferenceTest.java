package com.example.demo;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.DateTimeException;
import java.util.Date;

import org.junit.Assert;
import org.junit.jupiter.api.Test;




public class DateDifferenceTest {
	
	@Test//(expected = RuntimeException.class)
	//public void testDateDifferenceifDatesAreNull() {
		//DateDifference d=new DateDifference();
		//d.calculateDataDifferance(null,new Date());
	//}
	
	public void testDateDifference() {
		DateDifference d=new DateDifference();
		Date d1 = new Date(1662658592032l);
		Date d2 = new Date(1662658572032l);
		Assert.assertEquals(20000, d.calculateDataDifferance(d1,d2));
		
	}
	
}