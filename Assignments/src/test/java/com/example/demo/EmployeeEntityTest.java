package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;


public class EmployeeEntityTest {
	@Test
	public void testSortWithoutLambda() {

		EmployeeSort employeeSort = new EmployeeSort();
		List<Employ> emploies = getEmployees();
		employeeSort.sortEmployee(emploies);
		validateData(emploies);

	}

	@Test
	public void testSortWithLambda() {
		EmployeeSort employeeSort = new EmployeeSort();
		List<Employ> emploiesLambda = getEmployees();
		employeeSort.SortEmployeesWithLambda(emploiesLambda);
		validateData(emploiesLambda);

	}

	private void validateData(List<Employ> emploies) {
		Assert.assertEquals("Lakshmi", emploies.get(0).getName());
		Assert.assertEquals("Laxman", emploies.get(1).getName());
		Assert.assertEquals("Nani", emploies.get(2).getName());
		Assert.assertEquals("sneha", emploies.get(3).getName());

	}

	private List<Employ> getEmployees() {
		List<Employ> emploies = new ArrayList<>();
		Employ emp = new Employ();
		emp.setId("1");
		emp.setName("sneha");
		emp.setDepartment("IT");
		Employ emp1 = new Employ();
		emp1.setId("2");
		emp1.setName("Laxman");
		emp1.setDepartment("IT");
		Employ emp2 = new Employ();
		emp2.setId("3");
		emp2.setName("Lakshmi");
		emp2.setDepartment("HR");
		Employ emp4 = new Employ();
		emp4.setId("3");
		emp4.setName("Nani");
		emp4.setDepartment("HR");
		emploies.add(emp1);
		emploies.add(emp2);
		emploies.add(emp);
		emploies.add(emp4);

		return emploies;

	}

}
