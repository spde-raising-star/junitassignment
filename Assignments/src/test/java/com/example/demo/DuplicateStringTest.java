package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DuplicateStringTest {

	@Test
	void test() {
		
		assertEquals(1,DuplicateString.countofDuplicate("aab"));
	}

}
