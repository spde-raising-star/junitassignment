package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;



class Question1Test {

	@Test
	void nonStaticTest() {
		Question1 q1 = new Question1();
		System.out.println(q1.countNonStatic);
		assertNotEquals(3, q1.countNonStatic);}

	@Test
	  void staticTest() {
		Question1 p1 = new Question1();
	    Question1 p2 = new Question1();
		Question1 p3 = new Question1();
		System.out.println(p3.countStatic);
      assertEquals(3, p3.countStatic);}
}
