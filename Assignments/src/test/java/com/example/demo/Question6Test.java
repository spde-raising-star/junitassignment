package com.example.demo;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class Question6Test {
	Question6 que = new Question6();

	@Test
	void testSaveEvenNumbers() {
		List<Integer> expectedList = Arrays.asList(2, 4, 6, 8, 10);
		assertArrayEquals(expectedList.toArray(), que.saveEvenNumbers(10).toArray());

	}

	@Test
	void testPrintEvenNumbers() {
		ArrayList<Integer> inputList = que.saveEvenNumbers(10);
		List<Integer> resultList = Arrays.asList(4, 8, 12, 16, 20);
		assertArrayEquals(resultList.toArray(), que.printEvenNumbers(inputList).toArray());
	}
}