package com.example.demo;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.example.demo1.EmployeeEntity;
import com.example.demo1.EmployeeRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class EmployeeEntityTests {
	@Autowired
    private EmployeeRepository employeeRepository;
    

    @Test
    public void testcreateEntity() {
    	
    	EmployeeEntity emp = new EmployeeEntity("sneha","latha","sne@gmail.com");
    	
    	
    	EmployeeEntity savedemp = employeeRepository.save(emp);
    	assertNotNull(savedemp);
        
    	
		 }
}